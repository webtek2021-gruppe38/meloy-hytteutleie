window.onload = function() {
    setDate();
};
function setDate(){
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //Januar er 0 så må legge til 1
    let yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

    today = yyyy+'-'+mm+'-'+dd;
    document.getElementById("fra").setAttribute("min", today);
    document.getElementById("fra").setAttribute("value", today);
    setMinToDate();
}

function setMinToDate(){
    let from = document.getElementById("fra").value;
    let to = new Date(from)
    to.setDate(to.getDate() + 1);

    let dd = to.getDate();
    let mm = to.getMonth()+1; //Januar er 0 så må legge til 1
    let yyyy = to.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

    to = yyyy+'-'+mm+'-'+dd;

    document.getElementById("til").setAttribute("min", to);
    document.getElementById("til").disabled = false;
    document.getElementById("til").setAttribute("value", to);
    calcPrice();
}

function displayHytte(){
    if(document.getElementById("h1").selected == true){
        document.getElementById("image").setAttribute("src", "/img/cabins/01/01.jpg")
    }
    else if(document.getElementById("h2").selected == true){
        document.getElementById("image").setAttribute("src", "/img/cabins/02/01.jpg")

    }
    else if(document.getElementById("h3").selected == true){
        document.getElementById("image").setAttribute("src", "/img/cabins/03/01.jpg")
    }
    calcPrice();
}

function calcPrice(){
    let from = new Date(document.getElementById("fra").value);
    let to = new Date(document.getElementById("til").value);

    if(!Date.parse(to)){
        return;
    }

    if(document.getElementById("h1").selected == true){
        var price = (to.getDate() - from.getDate())*550;
    }
    else if(document.getElementById("h2").selected == true){
        var price = (to.getDate() - from.getDate())*500;
    }
    else if(document.getElementById("h3").selected == true){
        var price = (to.getDate() - from.getDate())*600;
    }
    else{var price = 0}
    
    document.getElementById("pris").value = price+"kr";
    console.log(price);
}

const conf = document.getElementById("confirm");
new URLSearchParams(window.location.search).forEach((value, name) => {
    if(name == "fra" || name == "til"){
        conf.append(`${name}: ${value}`)
        conf.append(document.createElement('br'))
        conf.append(document.createElement('br')) 
    }
    else if(name == "hytte"){
        conf.append(`du har booket ${value}`)   
        conf.append(document.createElement('br'))
        conf.append(document.createElement('br')) 
    } 
})

new URLSearchParams(window.location.search).forEach((value, name) => {
    if(value == "hytte 1"){
        document.getElementById("image2").src = "/img/cabins/01/01.jpg";
    }
    else if(value == "hytte 2"){
        document.getElementById("image2").src = "/img/cabins/02/01.jpg";
    }
    else if(value == "hytte 3"){
        document.getElementById("image2").src = "/img/cabins/03/01.jpg";
    }
    
})
