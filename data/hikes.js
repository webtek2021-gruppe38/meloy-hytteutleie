export const hikes = {
  "01": {
    name: "Oppdal - Storhornet",
    details: {
      Tid: "2 timer og 30 minutter",
      Distanse: "5.8km",
      Vansklighetsgrad: "Krevende",
      Sesong: "Hele året",
      Type: "Fottur",
    },
    description:
      "Fjellet ligger majestetisk til vest i Oppdal. Oppdals mest besøkte fjell og et av Midt-Norges mest populære turmål. Vinteren er høysesong for turen til Storhornet, men kan også være en fin tur sommerhalvåret.\n\nStartpunktet er parkeringsplassen ved gården Breen (p-avgift). Stien oppover bjørkeskogen er godt merket og den følger parallelt med Breesbekken. Du kommer etterhvert over skoggrensen og får fin utsikt over Oppdal, videre oppover kommer du til mosekled underlag som er lett å gå på. Når du nærmer deg toppen blir det litt mer steinete men greit å komme over. På toppen av Storhornet finner du ei åpen steinhytte med gjestebok. Hytta er god som et krypinn på kalde dager for en matbit eller klesskifte. På fine dager er det flott å sette seg i solveggen ved hytta og nyte utsikten fra toppen. Panoramautsikt over Trollheimen, Sunndalsfjella, Snøhetta og Oppdal. Retur samme vei som ruta opp.\n\nPå vinteren er det merket vinterløype opp til toppen.",
  },
  "02": {
    name: "Røros - Enarsvola ",
    details: {
      Tid: "2 timer",
      Distanse: "4.5 km",
      Vansklighetsgrad: "Enkel",
      Sesong: "Jun-sept",
      Type: "Fottur",
    },
    description:
      "En god ettermiddagstur i lett terreng. Enarsvola ligger 4 km (i luftlinje) nordøst for Røros Hotel. Det er godt merket sti fra parkeringsplassen ovenfor hotellet, ved Konstknektveien. Turen går i åpent lende med fin utsikt. Når du kommer til Enarsvolas høyeste punkt, møter du fjellkjerka - et godt sted å puste ut etter motbakken. Fra fjellkjerka har du fin utsikt, selv om du ikke har klatret spesielt høyt. Dette er en fin ettermiddagstur for både store og små.",
  },
  "03": {
    name: "Åre - Lillåstugan",
    details: {
      Tid: "0.5-1 time",
      Distanse: "4.0 km",
      Vansklighetsgrad: "Enkel",
      Sesong: "Vinter",
      Type: "Skitur",
    },
    description:
      "Dette er en lett tur som starter fra Ullådalens øvre parkering og går direkte til vaffelstuen Lillåstugan (som selvsagt har både mer mat og annet godt). Det går litt opp og ned så turen passer best for de som klarer å bremse i nedoverbakker og gå fiskebein i motbakker. De fleste klarer denne turen. Det går an å gjøre turen litt lengre ved å følge 5-kilometersporet etter å ha kjørt over Ullån.",
  },
};
