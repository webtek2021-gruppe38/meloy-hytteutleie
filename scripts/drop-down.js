// Liste med alle sidene som skal linkes til i navigasjonsmenyen
var pages = ["/", "/pages/turer/", "/pages/faq"];

// Liste av navnene på alle sidene som skal vises i menyen
var names = ["hytter", "turer", "ofte stilte spørsmål"];

/* Funksjon som lager navigasjonsmenyen. Legger til listen av sidene og en toggle-knapp 
 for dropdownmeyen til navbar elementet */

function makeNavbar() {
  console.log("making navbar");
  var counter = 1;
  var menu = document.createElement("ul");
  menu.setAttribute("id", "navmenu");
  menu.setAttribute("class", "navmenu");
  for (var i = 0; i < pages.length; i++) {
    var node = document.createElement("li");
    node.setAttribute("class", "navelement");

    var navlink = document.createElement("a");
    navlink.href = pages[i];
    node.appendChild(navlink);

    if (counter == 1) {
      node.setAttribute("id", "logo");
    }

    menu.appendChild(node);
    var text = names[i];
    navlink.appendChild(document.createTextNode(text));

    document.getElementById("navbar").appendChild(menu);
    counter += 1;
  }

  var toggle = document.createElement("i");
  toggle.className = "fas fa-bars fa-lg";
  toggle.setAttribute("id", "toggleId");
  toggle.setAttribute("onclick", "showDropdown()");

  document.getElementById("navbar").appendChild(toggle);
  makeDropDownMenu();
}

/* Funksjon somo lager dropdoownmenyen, ved å lage en div og legge til alle elementene i den */
function makeDropDownMenu() {
  var div = document.createElement("ul");
  div.setAttribute("id", "dropdownmenu");
  div.setAttribute("class", "dropdownmenu-hidden");

  for (var i = 0; i < pages.length; i++) {
    var menu = document.createElement("ul");
    var node = document.createElement("li");
    var navlink = document.createElement("a");
    navlink.href = pages[i];
    node.appendChild(navlink);
    menu.appendChild(node);
    var text = names[i];
    navlink.appendChild(document.createTextNode(text));

    div.appendChild(node);
    document.getElementById("navbar").appendChild(div);
  }
}

/* Dropdownmenyen blir synlig når man trykker på toggle knappen */
function showDropdown() {
  console.log("toggle");
  var menu = document.getElementById("dropdownmenu");
  if (menu.className === "dropdownmenu") {
    menu.className = "dropdownmenu-hidden";
  } else if (menu.className === "dropdownmenu-hidden") {
    menu.className = "dropdownmenu";
  }
}

// Når siden lastes blir navigasjonsbaren lagt til
window.onload = makeNavbar();

/* Funksjon som highlighter den siden man er på i menybaren 
 /* Inspirert av dette eksempelet på StackOverflow:
      https://stackoverflow.com/questions/6964503/using-javascript-to-highlight-current-page-in-navbar
 */
var url = location.href.split("/");
var navLinks = document
  .getElementsByTagName("nav")[0]
  .getElementsByClassName("navelement");
var currentPage = url[url.length - 1];

for (var i = 0; i < navLinks.length; i++) {
  var lb = navLinks[i].firstChild.href.split("/");
  if (lb[lb.length - 1] == currentPage) {
    navLinks[i].className = "current";
  }
}
