//first step is to get the modal
window.onload = () => {
  var modal = document.getElementById("myModal");

  //getting the images and inserting it inside the modal
  var images = document.getElementsByClassName("myImages");
  var modalImg = document.getElementById("img01");
  var captionText = document.getElementById("caption");
  for (var i = 0; i < images.length; i++) {
    var img = images[i.toString()];
    img.onclick = function (evt) {
      console.log("test");
      modal.style.display = "block";
      modalImg.src = this.src;
      captionText.innerHTML = this.alt;
    };
  }

  // get element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  //when user clicks on close, close the modal
  span.onclick = function () {
    modal.style.display = "none";
  };
};
