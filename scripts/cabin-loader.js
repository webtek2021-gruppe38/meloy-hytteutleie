import { getID } from "./url-parameters.js";
import { cabins } from "../data/cabins.js";

export function getCabins() {
  return cabins;
}

export function getCabinsAsArray() {
  return Object.entries(cabins).map(([key, value]) => ({
    id: key,
    ...value,
  }));
}

export function getCabin(id) {
  return cabins[id];
}

/* not used */
export function addCabinToElement(cabinId, containerId) {
  const currentHike = getCabins(cabinId);
  const hikeContainer = document.getElementById(containerId);
  const imgrow = document.createElement("div");
  const img1 = document.createElement("img");
  const img2 = document.createElement("img");
  const img3 = document.createElement("img");
  const card = document.createElement("div");
  const title = document.createElement("h1");
  const description = document.createElement("div");
  card.className = "card";
  title.innerText = currentHike.name;
  description.innerText = currentHike.description;
  description.className = "pad clm-2";
  img1.src = `/img/turer/${hikeId}/01.jpg`;
  img2.src = `/img/turer/${hikeId}/02.jpg`;
  img3.src = `/img/turer/${hikeId}/03.jpg`;
  imgrow.className = "card-imgrow";
  imgrow.appendChild(img1);
  imgrow.appendChild(img2);
  imgrow.appendChild(img3);
  card.appendChild(imgrow);
  card.appendChild(title);
  card.appendChild(description);
  hikeContainer.appendChild(card);
}

export function addCabinDetailsToElement(id) {
  const cabinContainer = document.getElementById(id);
  const cabin = { id: getID(), ...getHike(getID()) };
  /* Create needed elements */
  const card = document.createElement("div");
  const img = document.createElement("img");
  const title = document.createElement("h1");
  const details = document.createElement("ul");
  const hyperlink = document.createElement("a");
  const button = document.createElement("button");

  /* Set element content  and stylings*/
  button.innerText = "Les mer";
  hyperlink.href = `/pages/hytte/?id=${cabin.id}`;
  Object.values(cabin.details).forEach((value) => {
    const detail = document.createElement("li");
    detail.appendChild(keyElement);
    detail.append(value);
    details.appendChild(detail);
  });
  card.className = "card";
  img.src = `/img/turer/${cabin.id}/kart.png`;
  title.textContent = cabin.name;

  /* Append elements to correct parent */
  card.appendChild(img);
  card.appendChild(title);
  card.appendChild(details);
  cabinContainer.appendChild(card);
}

export function addCabinsToElement(id) {
  const cabinContainer = document.getElementById(id);
  getCabinsAsArray().forEach((cabin) => {
    /* Create needed elements */
    const card = document.createElement("div");
    const img = document.createElement("img");
    const title = document.createElement("h1");
    const details = document.createElement("ul");
    const hyperlink = document.createElement("a");
    const button = document.createElement("button");
    const footer = document.createElement("footer");

    /* Set element content  and stylings*/
    button.innerText = "Les mer";
    hyperlink.href = `/pages/hytte/?id=${cabin.id}`;
    Object.values(cabin.details).forEach((value) => {
      const detail = document.createElement("li");
      detail.append(value);
      details.appendChild(detail);
    });
    const priceElement = document.createElement("li");
    priceElement.className = "bold";
    priceElement.textContent = cabin.price;
    details.appendChild(priceElement);
    card.className = "card";
    img.src = `/img/cabins/${cabin.id}/01.jpg`;
    img.className = "myImages";
    title.textContent = cabin.name;

    /* Append elements to correct parent */
    hyperlink.appendChild(button);
    card.appendChild(img);
    card.appendChild(title);
    card.appendChild(details);
    card.appendChild(footer);
    footer.appendChild(hyperlink);
    cabinContainer.appendChild(card);
  });
}

export function addCabinDescriptionToElement(elementId) {
  const cabin = cabins[getID()];
  document.getElementById(elementId).innerText = cabin.description;
}

export function addCabinNameToElement(elementId) {
  const cabin = cabins[getID()];
  document.getElementById(elementId).innerText = cabin.name;
}
