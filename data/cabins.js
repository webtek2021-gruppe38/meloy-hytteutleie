export const cabins = {
  "01": {
    name: "Røroshytta",
    description:
      "Drøye 2,5 time unna Trondheim finner du den koselige og tradisjonsrike bergstaden Røros. Meløy hytteutleies Røroshytte er lokalisert bare 2 km fra Røros sentrum i et flott turområde. Hytta byr på ca. 80 kvadratmeter, og har én romslig stue, tre soverom og ett bad. Kjøkken og bad er nylig oppusset, noe som gjør at hytta også kan by på innlagt vann. Røroshytta ligger idyllisk til 3 km fra Røros sentrum. Det er en tradisjonsrik hytte med koselig innredning og en behagelig atmosfære. Her kan du virkelig få deg et avbrekk fra hverdagen. ",
    details: [
      "3km fra sentrum",
      "Wifi inkludert",
      "4 sengeplasser, en dobbeltseng og to enkeltsenger ",
      "Innlagt strøm og vann",
      "Koselig stue med peis",
      'Liten "terasse" med bålpanne',
      "Skibod",
      "Alpinanlegg lokalisert 5minutters kjøring unna",
      "Liten time kjøring østover finner du Sverige",
      "Muligheter for kajakk, kano, fisk, sykkel mm. på sommerstid",
    ],
    price: "Fra kr 550,- per natt",
  },
  "02": {
    name: "Oppdalshytta",
    description:
      "Oppdalshytta er lokalisert bare 1 time og 50 minutter unna Trondheim, og befinner seg mer eller mindre midt i skiløypa. Her er det bare å spenne på seg skiene rett utenfor hytteveggen, så er du i gang! Hytta i Oppdal kan i tillegg by på svært kort avstand til alpinanlegget, som også er et sikkerstikk på vinters tid. Denne laftede hytten har alt man skulle trenge for en koselig og innholdsrik helg; den har to soverom og to bad, samt en svært koselig stue med peis. Hytta i Oppdal passer nok spesielt godt for deg som ønsker en god avkobling fra hverdagen og som ønsker å oppleve den ekte, tradisjonelle hyttefølelsen. Dette er hytta for deg som ønsker skikkelig hyttefølelse, ikke bare feriefølelse. Oppdalshytta er ekvivalent med det enkle hytteliv, og byr derfor ikke på wifi, og det er heller ikke innlagt strøm og vann. Ellers befinner hytta seg i fine omgivelser godt tilrettelagt for varierte aktiviteter sommer som vinter.",
    details: [
      "Kort avstand til aplinanlegg (0.5km)",
      "Ikke Wifi",
      "4 sengeplasser fordelt på 2 rom",
      "Ikke innlagt strøm",
      "Stue med peisovn",
      'Koselig uteareal med bålpanne',
      "Skiløype rett utenfor",
      "Variert og lekent stinett for sykling på sommeren",
      "Nærhet til Oppdal sentrum med blant annet to shoppingssentre",
      "Røyking tillates ikke",
    ],
    price: "Fra kr 500,- per natt",
  },
  "03": {
    name: "Årehytta",
    description:
      "Falken Lodge er en herlig hytte lokalisert sentralt i Åre sentrum. Åre er et av Skandinavias største skisteder i antall heiser og nedfarter, og er et godt valg både for den erfarne og litt mindre erfarne skikjører. I tillegg frister området med god mat og heftig afterski. Selve hytta byr på 6 sengeplasser, et funksjonelt kjøkken, en koselig innredet stue og to bad.",
    details: [
      "1 km fra skianlegg",
      "Wifi inkludert",
      "6 sengeplasser, en dobbeltseng og fire enkeltsenger",
      "Innlagt strøm og vann",
      "Kjøkken med integrerte hvitevarer",
      'Vaskemaskin for klær',
      "Kabel-TV m. muligheter for streaming fra mobil",
      "Sykkelstier, klatreparker og golfbaner er bare noen av mange aktuelle aktiviteter på sommerstid",
      "Røyking tillates ikke",
    ],
    price: "Fra kr 600,- per natt",
  },
};
