import { getID } from "./url-parameters.js";
import { hikes } from "../data/hikes.js";

export function getHikes() {
  return hikes;
}

export function getHikesAsArray() {
  return Object.entries(hikes).map(([key, value]) => ({
    id: key,
    ...value,
  }));
}

export function getHike(id) {
  return hikes[id];
}

export function addHikeToElement(elementId) {
  const hikeId = getID();
  const currentHike = getHike(hikeId);
  const hikeContainer = document.getElementById(elementId);
  const imgrow = document.createElement("div");
  const img1 = document.createElement("img");
  const img2 = document.createElement("img");
  const img3 = document.createElement("img");
  const card = document.createElement("div");
  const title = document.createElement("h1");
  const description = document.createElement("div");
  card.className = "card";
  title.innerText = currentHike.name;
  description.innerText = currentHike.description;
  description.className = "pad clm-2";
  img1.src = `/img/hikes/${hikeId}/01.jpg`;
  img2.src = `/img/hikes/${hikeId}/02.jpg`;
  img3.src = `/img/hikes/${hikeId}/03.jpg`;
  imgrow.className = "card-imgrow";
  imgrow.appendChild(img1);
  imgrow.appendChild(img2);
  imgrow.appendChild(img3);
  card.appendChild(imgrow);
  card.appendChild(title);
  card.appendChild(description);
  hikeContainer.appendChild(card);
}

export function addHikeDetailsToElement(id) {
  const hikeContainer = document.getElementById(id);
  const hike = { id: getID(), ...getHike(getID()) };
  /* Create needed elements */
  const card = document.createElement("div");
  const img = document.createElement("img");
  const title = document.createElement("h1");
  const details = document.createElement("ul");
  const hyperlink = document.createElement("a");
  const button = document.createElement("button");

  /* Set element content  and stylings*/
  button.innerText = "Les mer";
  hyperlink.href = `/pages/tur/?id=${hike.id}`;
  Object.entries(hike.details).forEach(([key, value]) => {
    const detail = document.createElement("li");
    const keyElement = document.createElement("span");
    keyElement.className = "bold capitalized";
    console.log(keyElement.classList);
    keyElement.textContent = `${key}: `;
    detail.appendChild(keyElement);
    detail.append(value);
    details.appendChild(detail);
  });
  card.className = "card";
  img.src = `/img/hikes/${hike.id}/kart.png`;
  title.textContent = hike.name;

  /* Append elements to correct parent */
  card.appendChild(img);
  card.appendChild(title);
  card.appendChild(details);
  hikeContainer.appendChild(card);
}

export function addHikesToElement(id) {
  const hikeContainer = document.getElementById(id);
  getHikesAsArray().forEach((hike) => {
    /* Create needed elements */
    const card = document.createElement("div");
    const img = document.createElement("img");
    const title = document.createElement("h1");
    const details = document.createElement("ul");
    const hyperlink = document.createElement("a");
    const button = document.createElement("button");
    const footer = document.createElement("footer");

    /* Set element content  and stylings*/
    button.innerText = "Les mer";
    hyperlink.href = `/pages/tur/?id=${hike.id}`;
    Object.entries(hike.details).forEach(([key, value]) => {
      const detail = document.createElement("li");
      const keyElement = document.createElement("span");
      keyElement.className = "bold";
      keyElement.textContent = `${key}: `;
      detail.appendChild(keyElement);
      detail.append(value);
      details.appendChild(detail);
    });
    card.className = "card";
    img.src = `/img/hikes/${hike.id}/01.jpg`;
    title.textContent = hike.name;

    /* Append elements to correct parent */
    hyperlink.appendChild(button);
    card.appendChild(img);
    card.appendChild(title);
    card.appendChild(details);
    card.appendChild(footer);
    footer.appendChild(hyperlink);
    hikeContainer.appendChild(card);
  });
}
